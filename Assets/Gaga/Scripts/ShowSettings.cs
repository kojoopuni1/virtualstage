﻿using System;
using System.Collections;
using System.Collections.Generic;
using LFL.Gaga.WebServices;
using UnityEngine;

namespace LFL.Gaga {
    [Serializable]

    // todo add menu item for selection of settings

    [CreateAssetMenu(fileName = "ShowSettings", menuName = "Gaga/ShowSettings", order = 1)]
    public class ShowSettings : ScriptableObject {

        private static ShowSettings _instance;

        public static ShowSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = Resources.Load<ShowSettings>("ShowSettings");

                    if (_instance == null)
                    {
                        Debug.LogError("Couldn't find show settings");
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// What is the unique identifier for this event?
        /// </summary>
        public static string EventId => Instance.eventId;

        [SerializeField]
        private string eventId = "GagaTestEvent";

        /// <summary>
        /// What service provider will we be using for database access etc.
        /// </summary>
        public static GagaWebServiceProvider WebServiceProvider => Instance.webServiceProvider;

        [SerializeField]
        private GagaWebServiceProvider webServiceProvider;

    }
}