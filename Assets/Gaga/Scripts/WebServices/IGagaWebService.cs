﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LFL.Gaga.WebServices {

    public interface IGagaWebService {

        event AudienceReactionDelegate AudienceReactionReceived;

        void Initialize(string eventId);

        void Close();

    }

    public delegate void AudienceReactionDelegate(object sender, string reactionType, Dictionary<string, object> parameters);
}