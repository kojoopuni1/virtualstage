﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Firestore;

namespace LFL.Gaga.WebServices {
    public class FirebaseService : IGagaWebService {

        private static FirebaseService _defaultInstance;

        private const string EventCollectionId = "events";
        private const string EventReactionsCollectionId = "Reactions";

        private const string DocumentKeyReactionType = "reaction";
        private const string DocumentKeyTimeStamp = "timestamp";
        private const string DocumentKeyParameters = "parameters";
        private FirebaseFirestore firestoreDB => FirebaseFirestore.DefaultInstance;
        private Action<string, Dictionary<string, object>> _updateAction;

        private FirebaseApp _app;

        private string _eventId;
        private bool _waitingForInitialState = true;
        private bool _hasInitialized;
        private ListenerRegistration _changeListener;

        public event AudienceReactionDelegate AudienceReactionReceived;

        private string EventReactionsCollectionPath {
            get {
                if (string.IsNullOrWhiteSpace(_eventId)) {
                    Debug.LogError("FirebaseService: eventID hasn't been set");
                    return null;
                }

                return string.Join("/",EventCollectionId, _eventId, EventReactionsCollectionId);
            }
        }

        public void Initialize(string eventId) {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available) {
                    // Create and hold a reference to the FirebaseApp, where app is a Firebase.FirebaseApp property of the application class.
                    _app = FirebaseApp.DefaultInstance;

                    _eventId = eventId;
                    _hasInitialized = true;

                    // Begin listening for audience reactions
                    Debug.Log($"FirebaseService:Initialize: Starting listening for audience reactions at {EventReactionsCollectionPath}");

                    Query query = firestoreDB.Collection(EventReactionsCollectionPath);

                    _changeListener = query.Listen(OnAudienceReactionUpdate);

                    //todo there is currently no guarantee that this collection exists - we need a way to see if we've successfully connected

                } else {
                    // Firebase Unity SDK is not safe to use.
                    Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
                }
            });
        }

        private void OnAudienceReactionUpdate(QuerySnapshot snapshot) {
            if (!_hasInitialized) {
                return;
            }

            if (snapshot.Metadata.IsFromCache) {
                return;
            }

            // The first snapshot of changes includes changes from before the show start
            if (_waitingForInitialState) {
                _waitingForInitialState = false;
                return;
            }

            List<DocumentChange> changes = new List<DocumentChange>(snapshot.GetChanges());

            int count = changes.Count;

            for (var i = 0; i < count; i++) {
                if (changes[i].ChangeType != DocumentChange.Type.Added) {
                    //todo is it possible to only query additions to begin with?
                    continue;
                }

                string reactionType;

                if (!changes[i].Document.TryGetValue(DocumentKeyReactionType, out reactionType)) {
                    Debug.LogWarning($"FireStoreReactionService: Couldn't get reactionType");
                    continue;
                }

                Timestamp timestamp;

                if (!changes[i].Document.TryGetValue(DocumentKeyTimeStamp, out timestamp)) {
                    Debug.LogWarning($"FireStoreReactionService: Couldn't get timestamp");
                    continue;
                }

                Dictionary<string, object> parameters;

                if (!changes[i].Document.TryGetValue(DocumentKeyParameters, out parameters)) {
                    Debug.LogWarning($"FireStoreReactionService: Couldn't get parameters");
                    continue;
                }

                AudienceReactionReceived?.Invoke(this, reactionType, parameters);
            }

        }

        public void Close() {
            Debug.Log("FirebaseService:Close");
            _changeListener?.Stop();
            _changeListener?.ListenerTask.Dispose();
            _app?.Dispose();
        }

    }
}
