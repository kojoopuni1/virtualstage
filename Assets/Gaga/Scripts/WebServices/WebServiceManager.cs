﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LFL.Gaga.WebServices {

    public enum GagaWebServiceProvider {
        Firebase
    }

    public static class WebServiceManager {

        private static bool _hasInitialized = false;

        public static IGagaWebService ActiveWebService {
            get {
                if (!_hasInitialized) {
                    Application.quitting += Quit;

                    if (_activeWebService != null) {
                        Debug.LogWarning("WebServiceManager:ActiveWebService: Web service already exists");
                        return _activeWebService;
                    }

                    _hasInitialized = true;
                    Debug.Log($"WebServiceManager:ActiveWebService: Starting service for {ShowSettings.WebServiceProvider.ToString()}");
                    switch ( ShowSettings.WebServiceProvider) {
                        case GagaWebServiceProvider.Firebase:
                            _activeWebService = new FirebaseService();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    _activeWebService.Initialize(ShowSettings.EventId);
                }

                return _activeWebService;
            }
        }

        private static void Quit() {
            _activeWebService?.Close();
        }

        private static IGagaWebService _activeWebService;



    }
}