﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LFL.Gaga.Rendering {


    public class WebCamDisplay : MonoBehaviour {
        void Start() {
            WebCamTexture webCamTexture = new WebCamTexture();
            this.GetComponent<MeshRenderer>().material.mainTexture = webCamTexture;
            webCamTexture.Play();
        }

    }
}