﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using UnityEngine.Rendering;

namespace LFL.Gaga.Rendering {

    [RequireComponent(typeof(MeshFilter))]
    //[RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MediaPlayer))]
    public class VideoShadow : MonoBehaviour {

#pragma warning disable 0649
        
        [SerializeField]
        private Material shadowCasterMaterial;

#pragma warning restore 0649
        
        void Start() {
            MeshFilter videoMeshFilter = GetComponent<MeshFilter>();
            MediaPlayer videoPlayer = GetComponent<MediaPlayer>();

            if (videoPlayer.m_WrapMode != TextureWrapMode.Repeat) {
                Debug.LogWarning($"VideoShadow: Overriding MediaPlayer TextureWrapMode on {gameObject.name} for shadow rendering");
                videoPlayer.m_WrapMode = TextureWrapMode.Repeat;
            }

            GameObject shadowCaster = new GameObject($"{gameObject.name}_ShadowCaster");
            shadowCaster.transform.parent = transform;
            shadowCaster.transform.localPosition = Vector3.zero;
            shadowCaster.transform.localRotation = Quaternion.identity;
            shadowCaster.transform.localScale = Vector3.one;

            MeshFilter shadowMeshFilter = shadowCaster.AddComponent<MeshFilter>();
            MeshRenderer shadowMeshRenderer = shadowCaster.AddComponent<MeshRenderer>();
            ApplyToMaterial shadowVideoReceiver = shadowCaster.AddComponent<ApplyToMaterial>();

            Material casterMaterial = new Material(shadowCasterMaterial);
            casterMaterial.name += "_clone";

            shadowMeshFilter.sharedMesh = videoMeshFilter.sharedMesh;

            shadowMeshRenderer.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            shadowMeshRenderer.sharedMaterial = casterMaterial;

            shadowVideoReceiver.Player = videoPlayer;
            shadowVideoReceiver.Material = casterMaterial;
        }
    }
}