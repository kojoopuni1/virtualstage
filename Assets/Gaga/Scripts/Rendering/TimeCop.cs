﻿using System.Collections;
using System.Collections.Generic;
using RenderHeads.Media.AVProVideo;
using UnityEngine;

namespace LFL.Gaga.Rendering {
	public class TimeCop : MonoBehaviour {
		private enum State {
			Loading,
			Playing,
			Finished,
		}

		public MediaPlayer myPlayer;

		public float toleranceMs = 33.37f;

		void Start() {

		}

		void LateUpdate() {
			if (IsVideoLoaded()) {
				return;
			}


			float authorityTime = TimeAuthority.TimeMS();

			float myTime = myPlayer.Control.GetCurrentTimeMs();
			float deltaTime = Mathf.Abs(authorityTime - myTime);
			if (deltaTime > toleranceMs) {
				myPlayer.Control.SeekFast(authorityTime + (toleranceMs * 0.5f)); // Add a bit to allow for the delay in playback start
				if (myPlayer.Control.IsPaused()) {
					myPlayer.Play();
				}
			}
		}

		private bool IsVideoLoaded() {
			return (myPlayer != null && myPlayer.Control != null && myPlayer.Control.HasMetaData() && myPlayer.Control.CanPlay() &&
			        myPlayer.TextureProducer.GetTextureFrameCount() > 0);
		}
	}
}