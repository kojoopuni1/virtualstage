﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace LFL.Gaga.Rendering {
    public class TimeAuthority : MonoBehaviour {


#pragma warning disable 0649
        [SerializeField]
        private PlayableDirector _authorityDirector;
#pragma warning restore 0649

        private static PlayableDirector AuthorityDirector;

        // Start is called before the first frame update
        void Awake() {
            AuthorityDirector = _authorityDirector;
        }

        public static int FrameTime() {
            if (AuthorityDirector == null) {
                return 0;
            }

            return Mathf.CeilToInt((float) (AuthorityDirector.time * 29.97f));
        }

        public static float TimeMS() {
            if (AuthorityDirector == null) {
                return 0;
            }

            return (float) (AuthorityDirector.time * 1000d);
        }

    }
}