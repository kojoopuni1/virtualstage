﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LFL.Gaga.AudienceReactions {

    public class SpinObjectReactionReceiver : AudienceReaction {
        private const string ParamKeySpeed = "speed";
        private const string ParamKeyObjectName = "objectName";

        //TODO this is just a placeholder to spin something while we think of a better implementation
        private bool _hasLookedForDemoObject = false;
        private bool _hasFoundDemoObject = false;
        private Transform _demoObject;

        private float _friction = 0.25f;
        private float _currentSpeed;

        public override void Activate(Dictionary<string, object> parameters) {

            float spinSpeed;

            if (!float.TryParse(parameters[ParamKeySpeed].ToString(), out spinSpeed)) {
                Debug.LogWarning("SpinObjectReactionReceiver: couldn't parse spin speed");
                return;
            }

            string objectName = parameters[ParamKeyObjectName] as string;

            if (objectName == null) {
                Debug.LogWarning("SpinObjectReactionReceiver: couldn't parse parameters");
                return;
            }

            Debug.Log($"SpinObjectReactionReceiver: {objectName} at speed {spinSpeed}");

            SetSpinSpeed(spinSpeed);
        }

        //TODO all the below content is just a placeholder to spin something while we think of a better implementation
        private void SetSpinSpeed(float newSpeed) {
            FindDemoObject();
            _currentSpeed = newSpeed;
        }

        private void FindDemoObject() {
            if (_hasLookedForDemoObject || _hasFoundDemoObject) {
                return;
            }

            _hasLookedForDemoObject = true;

            GameObject foundGameObject = GameObject.Find("Lights/Animated Stage Lights");
            if (foundGameObject == null) {
                return;
            }

            _demoObject = foundGameObject.transform;
            _hasFoundDemoObject = true;
        }

        private void Update() {
            if (!_hasFoundDemoObject) {
                return;
            }

            _currentSpeed -= _friction * _currentSpeed * Time.deltaTime;
            _demoObject.Rotate(Vector3.up, _currentSpeed * Time.deltaTime);
        }
    }

}