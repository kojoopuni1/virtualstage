﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LFL.Gaga.AudienceReactions
{
    [Serializable]
    public class EmojiConfig {
        public string friendlyName;
        public string unicode;
        public Material material;
        public Mesh overrideMesh;
        public ParticleSystem overrideParticleSystem;
    }

    public class EmojiReactionReceiver : AudienceReaction
    {
        private const string ParamKeyUnicode = "unicode";

#pragma warning disable 0649

        [SerializeField] private ParticleSystem defaultParticleSystem;

        [SerializeField] private EmojiConfig[] emojiConfigs;

#pragma warning restore 0649

        private Dictionary<string, ParticleSystem> emojiParticleSystems = new Dictionary<string, ParticleSystem>();

        private void Awake() {
            // create a child instance for each emoji type
            foreach (EmojiConfig config in emojiConfigs) {
                if (string.IsNullOrEmpty(config.unicode)) {
                    Debug.LogWarning($"EmojiReactionReceiver: emojiConfigs contains a config with an empty reaction identifier");
                    continue;
                }

                if (config.material == null) {
                    Debug.LogWarning($"EmojiReactionReceiver: emojiConfigs contains a config with a null material reference");
                    continue;
                }

                ParticleSystem reaction;
                if (config.overrideParticleSystem != null) {
                    reaction = Instantiate(config.overrideParticleSystem, transform);
                } else {
                    reaction = Instantiate(defaultParticleSystem, transform);
                    ParticleSystemRenderer newSystemRenderer = reaction.GetComponent<ParticleSystemRenderer>();

                    newSystemRenderer.sharedMaterial = config.material;

                    if (config.overrideMesh != null) {
                        newSystemRenderer.mesh = config.overrideMesh;
                    }
                }

                if (reaction != null) {
                    reaction.gameObject.name = config.unicode;
                    emojiParticleSystems.Add(config.unicode, reaction);
                } else {
                    Debug.LogWarning($"EmojiReactionReceiver: Could not instantiate ParticleSystem for {config.friendlyName}");
                    continue;
                }
            }

        }

        //TODO ability to set location of any audience reaction

        public override void Activate(Dictionary<string, object> parameters) {
            string unicode = parameters[ParamKeyUnicode] as string;

            if (unicode == null || !emojiParticleSystems.ContainsKey(unicode)){
                Debug.LogWarning("EmojiReactionReceiver: couldn't parse parameters");
                return;
            }

            emojiParticleSystems[unicode].Emit(1);
        }
    }

}