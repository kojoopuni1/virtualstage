﻿using System;
using System.Collections;
using System.Collections.Generic;
using LFL.Gaga.Timeline;
using UnityEngine;
using LFL.Gaga.WebServices;
using UnityEngine.Playables;

namespace LFL.Gaga.AudienceReactions {
    [Serializable]
    public class ReactionConfig {
        public string reactionType;
        public AudienceReaction prefab;
    }

    public class AudienceReactionReceiver : MonoBehaviour, INotificationReceiver {

#pragma warning disable 0649

        [SerializeField]
        private ReactionConfig[] reactionConfigs;

#pragma warning restore 0649

        private Transform _reactionsRoot;
        private Dictionary<string, AudienceReaction> audienceReactions = new Dictionary<string, AudienceReaction>();

        private void Awake() {

            DontDestroyOnLoad(this);

            _reactionsRoot = new GameObject("Reactions").transform;
            _reactionsRoot.parent = transform;

            // create a child instance for each reaction type
            foreach (ReactionConfig config in reactionConfigs) {
                if (string.IsNullOrEmpty(config.reactionType)) {
                    Debug.LogWarning($"AudienceReactionReceiver: reactionAssociations contains an association with an empty reaction identifier");
                    continue;
                }

                if (config.prefab == null) {
                    Debug.LogWarning($"AudienceReactionReceiver: reactionAssociations contains an association with a null prefab reference");
                    continue;
                }

                AudienceReaction reaction = Instantiate(config.prefab, Vector3.zero, Quaternion.identity, _reactionsRoot);
                if (reaction != null) {
                    audienceReactions.Add(config.reactionType, reaction);
                } else {
                    Debug.LogWarning($"AudienceReactionReceiver: Could not instantiate {config.prefab.name}");
                }
            }

            WebServiceManager.ActiveWebService.AudienceReactionReceived += ReactionReceived;
        }


        private void ReactionReceived(object sender, string reactionType, Dictionary<string, object> parameters) {
            Debug.Log($"AudienceReactionReceiver:ReactionReceived:{reactionType}");
            if (audienceReactions.ContainsKey(reactionType)) {
                audienceReactions[reactionType].Activate(parameters);
            } else {
                Debug.LogWarning($"AudienceReactionReceiver:ReactionReceived: Unknown reactionType: {reactionType}");
            }
        }

        private void OnDestroy() {
            WebServiceManager.ActiveWebService.AudienceReactionReceived -= ReactionReceived;
        }

        public void OnNotify(Playable origin, INotification notification, object context)
        {
            if(notification is AudienceReactionPositionEmitter reactionEmitter) {
                Debug.Log($"AudienceReactionReceiver:OnNotify: New Position: {reactionEmitter.newReactionPosition}");
                _reactionsRoot.position = reactionEmitter.newReactionPosition;
            }
        }
    }
}