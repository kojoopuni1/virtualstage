﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LFL.Gaga.AudienceReactions
{
    public abstract class AudienceReaction : MonoBehaviour
    {
        public abstract void Activate(Dictionary<string, object> parameters);
    }
}