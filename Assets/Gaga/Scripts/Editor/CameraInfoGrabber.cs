﻿using LFL.Gaga.Cameras;
using UnityEditor;
using UnityEngine;


namespace LFL.Gaga.Editor {

    public class CameraInfoGrabber : ScriptableWizard {

        public CameraManager switcherToPopulate;
        public Motion camMotion;

        [MenuItem("LFL/Tools/Camera Info Grabber")]
        static void CreateWizard() {
            ScriptableWizard.DisplayWizard<CameraInfoGrabber>("Camera Info Grabber", "Grab Info");
        }

        void OnWizardCreate() {
            GameObject[] objs = Selection.gameObjects;

            foreach (var obj in objs) {
                Camera[] cameras = obj.GetComponentsInChildren<Camera>(true);

                foreach (var cam in cameras) {
                    Debug.Log($"Cam: {cam.gameObject.name}");
                    // look in motion component for it's activation time

                    CameraInfo newCamInfo = new CameraInfo();

                    newCamInfo.focalLength = cam.focalLength;
                    newCamInfo.fieldOfView = cam.fieldOfView;
                    newCamInfo.sensorSize = cam.sensorSize;
                    newCamInfo.followTransform = cam.transform.parent;
                    

                    switcherToPopulate.AddToCamInfo(newCamInfo);
                }
            }
        }

    }
}