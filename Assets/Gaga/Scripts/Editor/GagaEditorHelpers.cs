﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LFL.Gaga.Editor {


    public static class GagaEditorHelpers {
        private const string SettingsPath = "Assets/Gaga/Resources/ShowSettings.asset";

        [MenuItem("LFL/Gaga/Settings")]
        private static void SelectSettings() {
            Object settingsInstance = AssetDatabase.LoadAssetAtPath<Object>(SettingsPath);
            if (settingsInstance == null) {
                Debug.LogWarning($"No Gaga settings file: creating one at: {SettingsPath}");
                settingsInstance = ScriptableObject.CreateInstance<ShowSettings>();
                AssetDatabase.CreateAsset(settingsInstance, SettingsPath);
            }

            Selection.activeObject = settingsInstance;

            EditorWindow[] allWindows = Resources.FindObjectsOfTypeAll<EditorWindow>();
            for (int i = 0; i < allWindows.Length; i++)
            {
                if (string.Equals(allWindows[i].titleContent.text, "Inspector"))
                {
                    allWindows[i].Focus();
                    break;
                }
            }


        }
    }
}
