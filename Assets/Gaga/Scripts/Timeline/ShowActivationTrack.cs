﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

namespace LFL.Gaga.Timeline {
    [TrackColor(0f / 255f, 235f / 255f, 90f / 255f)]
    [TrackBindingType(typeof(GameObject))]
    [TrackClipType(typeof(ShowActivationClip))]
    public class ShowActivationTrack : TrackAsset {

    }
}