﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace LFL.Gaga.Timeline {
    [Serializable]
    public class ShowActivationClip : PlayableAsset, ITimelineClipAsset {
        [SerializeField]
        private ShowActivationBehavior template = new ShowActivationBehavior();

        public ClipCaps clipCaps => ClipCaps.All;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner) {
            return ScriptPlayable<ShowActivationBehavior>.Create(graph, template);
        }

    }

}