﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

namespace LFL.Gaga.Timeline {
    public class AudienceReactionPositionEmitter : SignalEmitter {
        public Vector3 newReactionPosition;
    }
}