﻿using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace LFL.Gaga.Timeline {

    [System.Serializable]
    public struct AllShowContent {
        public string name;
        public string type;
        public string filePath;
        public Vector3 position;
        public string prefabID;
        public int playbackTargetID;
        public float startTime;
        public float endTime;
        public float easeInDuration;
        public float easeOutDuration;
        public float volume;
        public float camFocalLength;
        public float camFieldOfView;
        public float camNearClipPlane;
        public float camFarClipPlane;
        public bool looping;
    }
}