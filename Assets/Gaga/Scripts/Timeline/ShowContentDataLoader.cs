using System.Collections.Generic;
using System.Linq;
using LFL.Gaga.AudienceReactions;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using UnityEngine.Rendering.Universal;

#if UNITY_EDITOR
using UnityEditor.Timeline;
#endif

namespace LFL.Gaga.Timeline {

    [RequireComponent(typeof(ShowContentDataGrabber))]
    public class ShowContentDataLoader : MonoBehaviour {
        private List<AllShowContent> _allShowData;

#pragma warning disable 0649

        [SerializeField]
        private List<AllShowContent> _showCamerasList;

        [SerializeField]
        private List<AllShowContent> _showLightsList;

        [SerializeField]
        private List<AllShowContent> _showScreensList;

        [SerializeField]
        private List<AllShowContent> _showStagePropsList;

        [SerializeField]
        private List<AllShowContent> _showFootageList;

        [SerializeField]
        private List<AllShowContent> _showReactionList;

        [SerializeField]
        private List<AllShowContent> _showAudioList;

        [SerializeField]
        PlayableDirector _playableDirector;

        [SerializeField]
        PlayableGraph _graph;

        [SerializeField]
        TimelineAsset _timeline;

        [SerializeField]
        private AudienceReactionReceiver audienceReactionReceiver;

        [SerializeField]
        private GameObject[] showCameras;

        [SerializeField]
        private GameObject[] showLights;

        [SerializeField]
        private GameObject[] showScreens;

        [SerializeField]
        private GameObject[] showStageProps;

        [SerializeField]
        private GameObject[] showFootage;

        private Camera cameraProperties;

#pragma warning restore 0649

        private void Awake() {
            UnloadShowData();
        }

        private void OnEnable() {
            _playableDirector = this.GetComponent<PlayableDirector>();
            _playableDirector.playableAsset = _timeline;
        }

        private void Start() {
            SortAndPlayShowData();

            PlayFromTimeline();
            Debug.Log("This length of this timeline is " + _playableDirector.duration);
        }

        private void OnDisable() {
            UnloadShowData();
        }

        private void OnApplicationQuit() {
            UnloadShowData();
            Debug.Log("On Application Quit");
        }

        public void PlayFromTimeline() {
            TimelineAsset selectedAsset;
            {
                selectedAsset = _timeline;
            }

            _playableDirector.Play(selectedAsset);
        }

        public void SortAndPlayShowData() {
            _allShowData = this.GetComponent<ShowContentDataGrabber>()._showContent;

            foreach (var showObjectData in _allShowData) {

                switch (showObjectData.type) {
                    case "camera":
                        _showCamerasList.Add(showObjectData);
                        break;
                    case "stageProp":
                        _showStagePropsList.Add(showObjectData);
                        break;
                    case "footage":
                        _showFootageList.Add(showObjectData);
                        break;
                    case "screen":
                        _showScreensList.Add(showObjectData);
                        break;
                    case "light":
                        _showLightsList.Add(showObjectData);
                        break;
                    case "audienceReactionPosition":
                        _showReactionList.Add(showObjectData);
                        break;
                    case "audioClip":
                        _showAudioList.Add(showObjectData);
                        break;
                    default:
                        Debug.LogError(showObjectData.type + " is not a supported show type.");
                        break;
                }
            }

            CreateReactionCues(_showReactionList);
            CreateAudioTracks(_showAudioList);

            CreateActivationClips(_showCamerasList, showCameras);
            CreateActivationClips(_showStagePropsList, showStageProps);
            CreateActivationClips(_showScreensList, showScreens);
            CreateActivationClips(_showLightsList, showLights);

        }

        public void UnloadShowData() // Clear out the Timeline once user gets out of Play Mode
        {
#if UNITY_EDITOR
            //var asset = TimelineEditor.inspectedAsset;  // This clears out all timelines in entire scene
            var asset = _timeline; // This only clears out the timeline attached to this gameobject
            if (asset == null)
                return;

            foreach (var track in asset.GetRootTracks()) {
                var clips = track.GetClips().ToArray();
                var markers = track.GetMarkers().ToArray();

                // Delete all Clips on Track
                foreach (var c in clips) {
                    asset.DeleteClip(c);
                }

                // Delete all Markers on Track
                foreach (var m in markers)
                    track.DeleteMarker(m);

                // Delete Track
                track.timelineAsset.DeleteTrack(track);
            }

            // Refresh Timeline
            TimelineEditor.Refresh(RefreshReason.ContentsAddedOrRemoved);
#endif
        }

        public void CreateActivationClips(List<AllShowContent> showList, GameObject[] prefabCollection) {
            GameObject prefabToCreate;

            foreach (var item in showList) {
                foreach (var prefab in prefabCollection) {
                    if (item.name == prefab.name || item.prefabID == prefab.name) {
                        prefabToCreate = prefab.gameObject;

                        ShowActivationTrack track = _timeline.CreateTrack<ShowActivationTrack>();
                        _playableDirector.SetGenericBinding(track, prefabToCreate);
                        TimelineClip clip = track.CreateClip<ShowActivationClip>();

                        // Setting General Properties
                        clip.displayName = item.name;
                        clip.start = item.startTime;
                        clip.duration = item.endTime == 0 ? ((_playableDirector.duration) - item.startTime) : (item.endTime - item.startTime);

                        // Setting Camera Properties if Applicable
                        if (item.type == "camera") {

                            if (prefabToCreate.GetComponent<Camera>() != null) {
                                cameraProperties = prefabToCreate.GetComponent<Camera>();
                            } else if (prefabToCreate.GetComponentInChildren<Camera>() != null) {
                                cameraProperties = prefabToCreate.GetComponentInChildren<Camera>();
                            } else Debug.LogError(prefabToCreate.name + " does not have a camera component attached.");

                            cameraProperties.usePhysicalProperties = true; // Make Physical Camera Active
                            cameraProperties.GetUniversalAdditionalCameraData().renderPostProcessing = true; // Make Sure Post Processing is Enabled.

                            if (item.camFocalLength != 0)
                                cameraProperties.focalLength = item.camFocalLength;
                            if (item.camFieldOfView != 0)
                                cameraProperties.fieldOfView = item.camFieldOfView;
                            if (item.camNearClipPlane != 0)
                                cameraProperties.nearClipPlane = item.camNearClipPlane;
                            if (item.camFarClipPlane != 0)
                                cameraProperties.farClipPlane = item.camFarClipPlane;
                        }

                        // Setting AVPro Properties if Applicable
                        if (item.type == "screen" || item.type == "footage") {
                            var mediaControl = prefabToCreate.GetComponentInChildren<RenderHeads.Media.AVProVideo.MediaPlayer>(true);

                            if (mediaControl != null && mediaControl) {
                                mediaControl.PlatformOptionsWindows.useLowLatency = true;
                                mediaControl.m_AutoOpen = true;
                                mediaControl.m_AutoStart = true;
                                mediaControl.m_Loop = item.looping;
                                mediaControl.m_VideoLocation = RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder;
                                mediaControl.m_VideoPath = item.filePath;
                                mediaControl.PlayMediaOnAppUnpause = true;
                            }

                        }
                    }
                }

            }

        }

        private void CreateReactionCues(List<AllShowContent> reactionList) {
            SignalTrack track = _timeline.CreateTrack<SignalTrack>();
            track.name = "AudienceReactionPositions";
            _playableDirector.SetGenericBinding(track, audienceReactionReceiver);

            SignalAsset audienceReactionPositionSignal = ScriptableObject.CreateInstance<SignalAsset>();
            audienceReactionPositionSignal.name = "AudienceReactionPositionSignal";

            for (int i = 0; i < reactionList.Count; i++) {
                AudienceReactionPositionEmitter newMarker = track.CreateMarker<AudienceReactionPositionEmitter>(reactionList[i].startTime);
                newMarker.newReactionPosition = reactionList[i].position;

                newMarker.asset = audienceReactionPositionSignal;
            }
        }

        private void CreateAudioTracks(List<AllShowContent> trackList) {

            for (int i = 0; i < trackList.Count; i++) {
                AudioClip audioClip = Resources.Load<AudioClip>(trackList[i].filePath);

                if (audioClip == null) {
                    Debug.LogWarning($"Unable to load audio clip '{trackList[i].name}' from Resources/{trackList[i].filePath}");
                    continue;
                }

                AudioSource clipAudioSource = gameObject.AddComponent<AudioSource>();
                clipAudioSource.spatialize = false;
                clipAudioSource.volume = trackList[i].volume;

                AudioTrack track = _timeline.CreateTrack<AudioTrack>();
                track.name = trackList[i].name;
                _playableDirector.SetGenericBinding(track, clipAudioSource);

                TimelineClip clip = track.CreateClip<AudioPlayableAsset>();
                AudioPlayableAsset clipPlayable = ScriptableObject.CreateInstance<AudioPlayableAsset>();

                clipPlayable.clip = audioClip;

                // Setting General Properties
                clip.asset = clipPlayable;
                clip.displayName = trackList[i].name;
                clip.start = trackList[i].startTime;
                clip.duration = trackList[i].endTime == 0
                    ? ((_playableDirector.duration) - trackList[i].startTime)
                    : (trackList[i].endTime - trackList[i].startTime);
                clip.easeInDuration = trackList[i].easeInDuration;
                clip.easeOutDuration = trackList[i].easeOutDuration;
            }
        }

        public static void Bind(PlayableDirector director, string trackName, Animator animator) {
            var timeline = director.playableAsset as TimelineAsset;
            foreach (var track in timeline.GetOutputTracks()) {
                if (track.name == trackName) {
                    director.SetGenericBinding(track, animator);
                    break;
                }
            }

        }


    }

}
