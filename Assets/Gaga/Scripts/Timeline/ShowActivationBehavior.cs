﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace LFL.Gaga.Timeline {
    [Serializable]
    public class ShowActivationBehavior : PlayableBehaviour {
        GameObject boundGameObject;

        public override void ProcessFrame(Playable playable, FrameData info, object playerData) {
            boundGameObject = playerData as GameObject;

            if (boundGameObject != null) {
                boundGameObject.SetActive(true);
            }

        }

        public override void OnBehaviourPause(Playable playable, FrameData info) {
            var duration = playable.GetDuration();
            var time = playable.GetTime();
            var count = time + info.deltaTime;

            if (!Application.isPlaying) {
                return;
            }

            if ((info.effectivePlayState == PlayState.Paused && count > duration) || Mathf.Approximately((float) time, (float) duration)) {
                // Execute your finishing logic here:
                if (boundGameObject.activeSelf) {
                    boundGameObject.SetActive(false);
                }

            }
        }
    }


}
