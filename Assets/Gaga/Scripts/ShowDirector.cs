﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.Video;
using RenderHeads;
using RenderHeads.Media;

namespace LFL.Gaga {

    public class ShowDirector : MonoBehaviour {

#pragma warning disable 0649

        [SerializeField]
        List<PlayableDirector> _playableDirectors;

        [SerializeField]
        List<TimelineAsset> _timelines;

        [SerializeField]
        GameObject _cameraAnimator;

        [SerializeField]
        GameObject _countdown;

        [SerializeField]
        GameObject _speaker;

        [SerializeField]
        GameObject _vfx;

        [SerializeField]
        GameObject _videoScreen;

#pragma warning restore 0649

        //
        public void Play() {
            foreach (PlayableDirector playableDirector in _playableDirectors) {
                playableDirector.Play();
            }

        }


        // Swap Timelines
        public void PlayFromTimelines(int index) {
            TimelineAsset selectedAsset;

            if (_timelines.Count <= index) {
                selectedAsset = _timelines[_timelines.Count - 1];
            } else {
                selectedAsset = _timelines[index];
            }

            _playableDirectors[0].Play(selectedAsset);
        }


        // Signal Events
        public void StartPresentation() {
            _cameraAnimator.GetComponent<Animator>().enabled = true;
            _countdown.GetComponent<VideoPlayer>().enabled = false;
            _speaker.gameObject.SetActive(true);
            _videoScreen.gameObject.SetActive(true);
            // _vfx.gameObject.SetActive(true);
        }

        public void EndPresentation() {
            _videoScreen.gameObject.SetActive(false);
        }

        private class ShowObjectData {
#pragma warning disable 0649

            public GameObject showPrefab;
            public Vector3 objectPosition;

#pragma warning restore 0649
        }
    }
}