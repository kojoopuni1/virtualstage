﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;

namespace LFL.Gaga.Cameras {
	public class CameraFacingVideoPlane : MonoBehaviour {
		[SerializeField]
		private Camera referenceCamera;

		public Transform target;

		public enum Axis {
			up,
			down,
			left,
			right,
			forward,
			back
		};

		public bool reverseFace = false;
		public Axis axis = Axis.up;

		public bool amActive = false;
		public bool autoInit = true;
		private GameObject _videoFrameContainer;

		public float smooth = 0.3f;
		public float distance = 5.0f;
		public float yVelocity = 0.0f;

		// return a direction based upon chosen axis
		public Vector3 GetAxis(Axis refAxis) {
			switch (refAxis) {
				case Axis.down:
					return Vector3.down;
				case Axis.forward:
					return Vector3.forward;
				case Axis.back:
					return Vector3.back;
				case Axis.left:
					return Vector3.left;
				case Axis.right:
					return Vector3.right;
			}

			// default is Vector3.up
			return Vector3.up;
		}

		void Awake() {
			// if no camera referenced, grab the main camera
			if (!referenceCamera)
				referenceCamera = Camera.main;

			if (autoInit == true) {
				amActive = true;
			}

			_videoFrameContainer = new GameObject();
			_videoFrameContainer.name = "Group_" + transform.gameObject.name;
			_videoFrameContainer.transform.position = transform.position;
			transform.parent = _videoFrameContainer.transform;
		}
		//Orient the camera after all movement is completed this frame to avoid jittering

		void LateUpdate() {
			float yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, referenceCamera.transform.eulerAngles.y, ref yVelocity, smooth);

			//rotates the object relative to the camera
			Vector3 targetPos = transform.position + referenceCamera.transform.rotation * (reverseFace ? Vector3.forward : Vector3.back) * yAngle;
			//Vector3 targetPos = transform.position + Quaternion.Euler(0, yAngle, 0) * new Vector3(0, 0, -distance);
			Vector3 targetOrientation = referenceCamera.transform.rotation * GetAxis(axis);
			transform.LookAt(targetPos, targetOrientation);
		}


	}
}