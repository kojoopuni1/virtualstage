﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LFL.Gaga.Cameras {
    public class SmoothCameraFollow : MonoBehaviour {
        //A simple smooth follow camera,
        // that follows the targets forward direction

        [SerializeField]
        private Camera referenceCamera;

        public enum Axis {
            up,
            down,
            left,
            right,
            forward,
            back
        };

        public bool reverseFace = false;
        public Axis axis = Axis.up;

        public bool amActive = false;
        public bool autoInit = true;

        Transform target;
        float smooth = 0.3f;
        float distance = 5.0f;
        float yVelocity = 0.0f;

        private void Awake() {
            if (!referenceCamera)
                referenceCamera = Camera.main;

            if (autoInit == true) {
                amActive = true;
            }
        }

        void Update() {
            // Damp angle from current y-angle towards target y-angle
            float yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, referenceCamera.transform.eulerAngles.y, ref yVelocity, smooth);
            // Position at the target
            Vector3 position = referenceCamera.transform.position;
            // Then offset by distance behind the new angle
            position += Quaternion.Euler(0, yAngle, 0) * new Vector3(0, 0, -distance);
            // Apply the position
            transform.position = position;

            // Look at the target
            transform.LookAt(referenceCamera.transform);
        }
    }
}
