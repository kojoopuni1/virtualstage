﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace LFL.Gaga.Cameras {

    [System.Serializable]
    public class CameraInfo {
        public float focalLength;
        public float fieldOfView;
        public Vector2 sensorSize;
        public int activationFrame;
        public Transform followTransform;
    }

    public class CameraManager : MonoBehaviour {

#pragma warning disable 0649
        [SerializeField]
        private Camera _mainCamera;
        
        [SerializeField]
        private PlayableDirector director;
        
        [SerializeField]
        private int timingOffset;

        [SerializeField]
        private List<CameraInfo> cameraInfoList;

#pragma warning restore 0649
        
        private int _camCount;
        private CameraInfo[] _camList;
        private int _currentCam = 0;

        public void AddToCamInfo(CameraInfo newInfo) {
            cameraInfoList.Add(newInfo);
        }

        private void Awake() {
            _camList = cameraInfoList.ToArray();
            _camCount = _camList.Length;

            int prevFrame = _camList[0].activationFrame;
            for (int i = 1; i < _camCount; i++) {
                if (prevFrame >= _camList[i].activationFrame) {
                    Debug.LogError($"CameraManager:Awake Camera info at index {i} has an activation frame earlier than a previous camera");
                }

                prevFrame = _camList[i].activationFrame;
            }

            ApplyCamInfo(0);
        }

        void LateUpdate() {
            if (_currentCam < _camCount - 1) {
                int frameTime = Mathf.CeilToInt((float) (director.time * 29.97f));

                for (int i = _currentCam + 1; i < _camCount; i++) {
                    if (frameTime > _camList[i].activationFrame + timingOffset) {
                        ApplyCamInfo(i);

                        break;
                    }
                }
            }

            _mainCamera.transform.position = _camList[_currentCam].followTransform.position;
            _mainCamera.transform.rotation = Quaternion.LookRotation(-_camList[_currentCam].followTransform.forward, _camList[_currentCam].followTransform.up);
        }

        private void ApplyCamInfo(int camIndex) {
            _currentCam = camIndex;

            _mainCamera.focalLength = _camList[camIndex].focalLength;
            _mainCamera.fieldOfView = _camList[camIndex].fieldOfView;
            _mainCamera.sensorSize = _camList[camIndex].sensorSize;
        }
    }
}
