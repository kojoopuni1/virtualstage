// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Silhouette"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Metalic("Metalic", 2D) = "gray" {}
		_Smoothness("Smoothness", Range( 0 , 1)) = 0.5
		[Normal]_Normal("Normal", 2D) = "bump" {}
		_NormalScale("Normal Scale", Range( 0 , 2)) = 1
		_SilhouetteColor("Silhouette Color", Color) = (0,0,0,0)
		_TransitionDistance("Transition Distance", Float) = 0
		_TransitionFalloff("Transition Falloff", Float) = -2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform float _NormalScale;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _TransitionDistance;
		uniform float _TransitionFalloff;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _SilhouetteColor;
		uniform sampler2D _Metalic;
		uniform float4 _Metalic_ST;
		uniform float _Smoothness;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float4 color33 = IsGammaSpace() ? float4(0,0,1,0) : float4(0,0,1,0);
			float3 ase_worldPos = i.worldPos;
			float clampResult13 = clamp( pow( ( distance( ase_worldPos , _WorldSpaceCameraPos ) / _TransitionDistance ) , _TransitionFalloff ) , 0.0 , 1.0 );
			float4 lerpResult30 = lerp( float4( UnpackScaleNormal( tex2D( _Normal, uv_Normal ), _NormalScale ) , 0.0 ) , color33 , clampResult13);
			o.Normal = lerpResult30.rgb;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 lerpResult11 = lerp( tex2D( _Albedo, uv_Albedo ) , _SilhouetteColor , clampResult13);
			o.Albedo = lerpResult11.rgb;
			float2 uv_Metalic = i.uv_texcoord * _Metalic_ST.xy + _Metalic_ST.zw;
			float4 lerpResult26 = lerp( tex2D( _Metalic, uv_Metalic ) , _SilhouetteColor , clampResult13);
			o.Metallic = lerpResult26.r;
			float4 temp_cast_4 = (_Smoothness).xxxx;
			float4 lerpResult29 = lerp( temp_cast_4 , _SilhouetteColor , clampResult13);
			o.Smoothness = lerpResult29.r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18000
-2464;533;2293;1305;2921.271;520.6116;1.753054;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;5;-1819.926,955.3522;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;4;-1915.927,1227.352;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;7;-1515.927,1403.351;Inherit;False;Property;_TransitionDistance;Transition Distance;6;0;Create;True;0;0;False;0;0;9.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;6;-1387.927,1035.352;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-1115.927,1419.351;Inherit;False;Property;_TransitionFalloff;Transition Falloff;7;0;Create;True;0;0;False;0;-2;1.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;9;-1147.927,1003.352;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;10;-843.9264,955.3522;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-1543.37,724.0565;Inherit;False;Property;_NormalScale;Normal Scale;4;0;Create;True;0;0;False;0;1;0;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-1325.747,-372.4679;Inherit;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;-1;None;8aba6bb20faf8824d9d81946542f1ce1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;33;-1993.905,108.7349;Inherit;False;Constant;_Color0;Color 0;8;0;Create;True;0;0;False;0;0,0,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;13;-548.7466,911.0093;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;14;-1802.549,-44.45434;Inherit;False;Property;_SilhouetteColor;Silhouette Color;5;0;Create;True;0;0;False;0;0,0,0,0;0.3158152,0.3693992,0.4433962,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;16;-1361.413,521.684;Inherit;False;Property;_Smoothness;Smoothness;2;0;Create;True;0;0;False;0;0.5;0.81;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;17;-1785.414,260.1394;Inherit;True;Property;_Metalic;Metalic;1;0;Create;True;0;0;False;0;-1;None;None;True;0;False;gray;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;21;-1117.378,625.8855;Inherit;True;Property;_Normal;Normal;3;1;[Normal];Create;True;0;0;False;0;-1;None;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;26;-344.2816,129.7714;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;29;-282.9246,424.2845;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;30;-191.7658,676.7242;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;11;-405.5144,-32.53152;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;98.94926,2.998463;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Silhouette;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;5;0
WireConnection;6;1;4;0
WireConnection;9;0;6;0
WireConnection;9;1;7;0
WireConnection;10;0;9;0
WireConnection;10;1;8;0
WireConnection;13;0;10;0
WireConnection;21;5;24;0
WireConnection;26;0;17;0
WireConnection;26;1;14;0
WireConnection;26;2;13;0
WireConnection;29;0;16;0
WireConnection;29;1;14;0
WireConnection;29;2;13;0
WireConnection;30;0;21;0
WireConnection;30;1;33;0
WireConnection;30;2;13;0
WireConnection;11;0;2;0
WireConnection;11;1;14;0
WireConnection;11;2;13;0
WireConnection;0;0;11;0
WireConnection;0;1;30;0
WireConnection;0;3;26;0
WireConnection;0;4;29;0
ASEEND*/
//CHKSM=19A2906896CCE8CEFDE14298D537D217801FBEC0